context('UI Login', () => {
    const root_url = "https://app.getweave.com"
    beforeEach(() => {
        cy.visit(root_url + '/admin/login');
    });

    it('fills the login form and redirects to the Weave Admin Portal', () => {
        cy.get('[name="username"]')
            .type(Cypress.env('LOGIN_TEST_USER'))
            .should('have.value', Cypress.env('LOGIN_TEST_USER'))
            .get('[name="password"]')
            .type(Cypress.env('LOGIN_TEST_PASS'))
            .should('have.value', Cypress.env('LOGIN_TEST_PASS'))
            .get('.btn').click()
            .location('pathname', {timeout: 10000}).should('contain', '/dashboard')
            .title().should('contain', 'Weave Admin Portal');
    });

    it('verifies the button is disabled with an empty username', () => {
        cy.get('[name="username"]')
            .should('have.value', '')
            .get('[name="password"]')
            .type(Cypress.env('LOGIN_TEST_PASS'))
            .should('have.value', Cypress.env('LOGIN_TEST_PASS'))
            .get('.btn').should('be.disabled');
    });

    it('verifies the login button is disabled with an empty password', () => {
        cy.get('[name="username"]')
            .type(Cypress.env('LOGIN_TEST_USER'))
            .should('have.value', Cypress.env('LOGIN_TEST_USER'))
            .get('[name="password"]')
            .should('have.value', '')
            .get('.btn')
            .should('be.disabled');
    });
});