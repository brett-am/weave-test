context('api calls for add user behavior', () => {
    const root_api_url = "https://api.weaveconnect.com/portal/v1"
    var run_id = btoa(Math.floor(Math.random() * 99999)).toString().replaceAll('=', '')
    beforeEach(() => {
        cy.login()
    });

    it('/users', () => {
        cy.request({
            method: 'GET',
            url: root_api_url + '/users',
            headers: {
                "Location-id": "516c79ab-5317-48a5-a7f4-1fe6a45bec42",
                Authorization: "Bearer " + window.localStorage.getItem("token")
            }
        })
    })

     it('/users/roles', () => {
         cy.request({
             method: 'GET',
             url: root_api_url + '/users/roles',
             headers: {
                 "Location-id": "516c79ab-5317-48a5-a7f4-1fe6a45bec42",
                 Authorization: "Bearer " + window.localStorage.getItem("token")
             }
         })

     });

     it('/userDetails/jobTitles', () => {
         cy.request({
             method: 'GET',
             url: root_api_url + '/userDetails/jobTitles',
             headers: {
                 "Location-id": "516c79ab-5317-48a5-a7f4-1fe6a45bec42",
                 Authorization: "Bearer " + window.localStorage.getItem("token")
             }
         })
     });

    it('/users/inviteUser', () => {
        cy.request({
            method: 'POST',
            url: root_api_url + '/users/inviteUser',
            headers: {
                "Location-id": "516c79ab-5317-48a5-a7f4-1fe6a45bec42",
                Authorization: "Bearer " + window.localStorage.getItem("token")
            },
            body: {

                    "Email":"brett+inviteUser"+run_id+Cypress.env('TEST_DOMAIN'),
                    "FirstName":run_id,
                    "LastName": run_id,
                    "MobileNumber":"","Roles":[13],
                    "JobTitles":["097f6f07-c7e2-40ff-80c0-04214a15dcdf"]

    }
        })
    });

});