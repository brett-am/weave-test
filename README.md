# Weave Technical Assessment

To build:
`npm install`
To run all the tests:
`./run.sh`
To directly open the cypress console:
`npm run cypress:open`

## Login
I tested the happy path login, and tested the null path (empty fields, verified that login was disabled.)
Future improvements can include boundary tests (testing length of emails/passwords, etc.) and security tests (allowed unicode characters, stuffing attacks, etc.)


## API calls
I used the inspector and Postman to build up the request tests.
They are currently set to default status 200 checks, but future improvements 
can be made by picking data boundaries and writing asserts on those in the 
api tests. 

For example, if there is a max number of users allowed with a particular jobTitle,
the tests can be enhanced to assert on the JSON response.
Example Pseudocode:

`cy.request( ... )
.its('body.<n>') // grab the nth thing in the response 
.as('users')
.then(function () ) ... (assert/expect users <= X  )`

## CI/CD/Containerization
Running in a Gitlab CI pipeline at https://gitlab.com/brett-am/weave-test
Uses a 3 node Kubernetes cluster in Google Cloud Platform to parallelize across test runners.

## Discussion
I didn't run into many issues with selectors, but a few were fragile or had duplicates, 
which means a future feature could break the tests if an identical class was used. I used handles that
were specific wherever possible, like `[name=<tag>]`

I think Weave needs a structured `data-test-id` specification, and this could be the currency for 
communication on tests and features in the back-and-forth with the devs. 

I also used a base64 encoding for the user ids of emails. This can be further enhanced by baking an increment
into the ID to avoid the rare instance there might be a collision, or using the id in a seeded database to 
test against custom data payloads.

## Timeline

This took me about 7-8 hours, spread around my current schedule the last two days. 
You'll see in the commit history I got stuck for the last few of those hours on a classic 
"works locally, but not in CI" bug. Cypress has a known regression (https://github.com/cypress-io/cypress/issues/15618) 
that causes a bug pattern in CI due to a specific version of Cypress. I resolved it by making a new context for CI
that is pinned to the appropriate version.

## Bugs

1 - Hitting enter after typing an email on "add user" closes the context, instead of going to the 
user details form. Hitting enter ought to result in going to the user details form.
Unless shortcuts are disabled intentionally for some reason, this seems like a bug.
2- Job Title and Roles/Responsibilities dropdown need to be clicked again to close. They should close after
the user doesn't interact, or reorganized in a way where one dropdown doesn't cover another.

## Automation requirements

### The automation must run via the UI within a browser
Does this using cypress with test cases in Javascript

### Your solution must produce a detailed and clean report file
uses a basic reporter:spec (defined in cypress.json), which can be tailed to a reporting dashboard (Datadog, Grafana, etc.)

## Functional requirements
### Tests are runnable from the command line
runs from ./run.sh

### Tests should be configured to run against the Chrome browser by default
configured.

### Testing service credentials and all configuration settings are passed as environment variables
credentials are stored in cypress.json as env variables.

## Bonus

### Tests are run in parallel
Gitlab configured to run on a kubernetes cluster ('weave-kube-test', 3 nodes)

### Tests run against mobile browsers
I didn't get to this, but the next steps would be to use a framework like Ionic to run Cypress against mobile devices.

### Tests run headlessly
Runs headless in CI and locally

### Tests capture screenshots of failures
Cypress will take screenshots of failures in `screenshots/` and 
compresses a video of test runs in the `videos/`

### Final Notes
solfar == my macbook

Thanks, I had fun doing this assessment!

