#!/usr/bin/env sh

# run the tests
./node_modules/.bin/cypress run \
--headless \
--browser chrome \
cypress/integration/tests/*.js